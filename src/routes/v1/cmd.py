from fastapi import FastAPI,APIRouter
from fastapi.responses import RedirectResponse
from typing import Optional
from pydantic import BaseModel
import shlex
import subprocess

class CommandRequest(BaseModel):
    cmd: Optional[str] = None

router = APIRouter()

@router.post("/cmd")
def on_cmd_received(request: CommandRequest):
    if request.cmd is None:
        return {"status": "failed"}

    try:
        cmd_list = shlex.split(request.cmd)
        
        result = subprocess.run(cmd_list, shell=True, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        # result = subprocess.Popen("ros2 topic list", shell=True)
        output = result.stdout.decode('utf-8')
        error = result.stderr.decode('utf-8')
        return {"status": "success", "request": f"{request} | output: {output}"}

    except Exception as e:
        return {"status": "failed", "error": f"{e}"}

@router.get("/ping")
def on_ping():
    return {"status": "success", "message": "pong"}