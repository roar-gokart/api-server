from fastapi import FastAPI
from fastapi.responses import RedirectResponse
from pydantic import BaseModel
from src.routes.v1.dev import router as dev_router
from src.routes.v1.cmd import router as cmd_router
app = FastAPI()
app.include_router(dev_router, prefix="/v1")
app.include_router(cmd_router)
class DevPayload(BaseModel):
    name: str

@app.get("/")
async def root():
    return RedirectResponse(url="/docs")

if __name__ == "__main__":
    import uvicorn
    uvicorn.run("main:app", host="0.0.0.0", port=8000, reload=True)
